pandas==1.2.4
networkx==2.5.1
google-cloud-bigquery[bqstorage,pandas]==2.16.0
google-cloud-bigquery-storage[pandas]==2.4.0
matplotlib==3.4.2
mplfinance==0.12.7a17
scipy==1.6.3