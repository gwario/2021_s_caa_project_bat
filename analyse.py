import sys
import time
import traceback
from multiprocessing import Pool, Manager
from typing import Dict, Optional

from matplotlib import pyplot as plt

from utils.arguments import check_mode_start_args, recreate_session, parse_args
from utils.data import load_secrets
from utils.session_config import SessionConfig
from utils.task import Task
from utils.tasks.calculate_aggregated_cumsum_transfers import CalculateAggregatedCumsumTransfers
from utils.tasks.calculate_aggregated_transfers import CalculateAggregatedTransfers
from utils.tasks.calculate_aggregated_transfers_rolling import CalculateAggregatedTransfersRolling
from utils.tasks.calculate_sort_head import CalculateSortHeadTransfers
from utils.tasks.calculate_token_net_graph_characteristics import CalculateTokenNetGraphCharacteristics
from utils.tasks.calculate_token_net_graph_core_decomposition import CalculateTokenNetGraphCoreDecomposition
from utils.tasks.calculate_token_net_graph_core_decomposition_rolling import \
    CalculateTokenNetGraphCoreDecompositionRolling
from utils.tasks.calculate_token_net_graph_cycles_rolling import CalculateTokenNetGraphCyclesRolling
from utils.tasks.collect_contract_prices import CollectContractPrices
from utils.tasks.collect_contract_transfers import CollectContractTransfers
from utils.tasks.construct_token_net_graph import ConstructTokenNetGraph
from utils.tasks.visualize_aggregated_cumsum_transfers import VisualizeAggregatedCumsumTransfers
from utils.tasks.visualize_aggregated_transfers_rolling import VisualizeAggregatedTransfersRolling
from utils.tasks.visualize_aggregated_transfers_rolling_auto_correlation import \
    VisualizeAggregatedTransfersRollingAutoCorrelation
from utils.tasks.visualize_animated_histogram_transferred_value_rolling import \
    VisualizeAnimatedHistogramTransferredValueRolling
from utils.tasks.visualize_core_decomposition_rolling import VisualizeCoreDecompositionRolling
from utils.tasks.visualize_core_decomposition_rolling_and_price import VisualizeCoreDecompositionRollingAndPrice
from utils.tasks.visualize_historgam_transferred_value import VisualizeHistogramTransferredValue
from utils.tasks.visualize_historgam_transferred_value_rolling import VisualizeHistogramTransferredValueRolling
# TODO introduce parameter to provide ffmpeg path
from utils.tasks.visualize_prices import VisualizePrices

if sys.platform == 'win32' or sys.platform == 'cygwin':
    plt.rcParams['animation.ffmpeg_path'] = 'C:\\Users\\Mario\\Downloads\\ffmpeg-4.4-full_build\\bin\\ffmpeg'


def executor(task):
    try:
        task.execute()
    except Exception as ex:
        traceback.print_exc()
        raise ex


def main(session: SessionConfig, secrets: Optional[Dict] = None):

    p = Pool(processes=1)
    manager = Manager()
    session.use(manager)

    start_time = time.perf_counter()
    print("[Main] Collecting data...")
    # collect via google bigquery lib does not work in pool and has to be executed before the other collection tasks!
    CollectContractTransfers(session=session, secrets=secrets).execute()
    collection_tasks = [
        (CollectContractPrices(session=session, secrets=secrets).using(manager),)
    ]
    results = p.starmap_async(executor, collection_tasks)
    results.get()
    start_time = Task.print_track("[Main] Collection of data", start_time)

    print("[Main] Processing data...")
    processing_tasks = [
        (ConstructTokenNetGraph(session=session).using(manager),),
    ]
    results = p.starmap_async(executor, processing_tasks)
    results.get()
    start_time = Task.print_track("[Main] Processing of data", start_time)

    # TODO
    # get weakly connected components
    # wcc = networkx.weakly_connected_components(g_restricted)
    # # get the largest weakly connected components
    # largest_wcc = max(wcc, key=len)

    print("[Main] Calculating data...")
    # NOTE: sum aggregation with peak values of 16.000.000.000 do not produce an overflow or cutoff!
    # For BAT this occurs with a window size of 60 days
    calculation_tasks = [
        (CalculateTokenNetGraphCharacteristics(session=session).using(manager),),

        (CalculateTokenNetGraphCoreDecomposition(session=session).using(manager),),

        # (CalculateTokenNetGraphCyclesRolling(session=session, window_size_step_width_days=(3, 3)).using(manager),),

        (CalculateTokenNetGraphCyclesRolling(session=session, window_size_step_width_days=(21, 7)).using(manager),),

        (CalculateTokenNetGraphCoreDecompositionRolling(session=session,
                                                        window_size_step_width_days=(14, 3)).using(manager),),
        # (CalculateTokenNetGraphCoreDecompositionRolling(session=session,
        #                                                 window_size_step_width_days=(21, 7)).using(manager),),

        (CalculateAggregatedTransfers(task_id='tf_agg_full',
                                      session=session,
                                      aggregations={'value': ['sum', 'count']}).using(manager),),

        (CalculateAggregatedCumsumTransfers(session=session,
                                            aggregations={'value': ['sum', 'count']}).using(manager),),

        (CalculateAggregatedTransfersRolling(task_id='tf_agg_roll_window-1d',
                                             session=session,
                                             aggregations={'value': ['sum', 'count']},
                                             window_size_days=1).using(manager),),

        (CalculateAggregatedTransfersRolling(task_id='tf_agg_roll_window-5d_ac',
                                             session=session,
                                             aggregations={'value': ['sum', 'count']},
                                             window_size_days=5).using(manager),),

        (CalculateAggregatedTransfersRolling(task_id='tf_agg_roll_window-60d',
                                             session=session,
                                             aggregations={'value': ['sum', 'count']},
                                             window_size_days=60).using(manager),),
        (CalculateAggregatedTransfersRolling(task_id='tf_agg_roll_khan-lee',
                                             session=session,
                                             aggregations={'value': ['sum', 'count']},
                                             window_size_days=21).using(manager),),

        (CalculateSortHeadTransfers(session=session, head=200, column='value', ascending=False).using(manager),),
    ]
    results = p.starmap_async(executor, calculation_tasks)
    results.get()
    start_time = Task.print_track("[Main] Calculation of data", start_time)

    print("[Main] Generating visualizations...")
    visualization_tasks = [
        (VisualizePrices(session=session, volume=True).using(manager),),

        (VisualizeCoreDecompositionRolling(session=session, window_size_step_width_days=(21, 7)).using(manager),),

        # (VisualizeCoreDecompositionRolling(session=session, window_size_step_width_days=(14, 3)).using(manager),),

        (VisualizeCoreDecompositionRollingAndPrice(session=session,
                                                   window_size_step_width_days=(21, 7)).using(manager),),

        # (VisualizeCoreDecompositionRollingAndPrice(session=session,
        #                                            window_size_step_width_days=(14, 3)).using(manager),),

        (VisualizeAggregatedCumsumTransfers(task_id='tf_agg_cumsum',
                                            session=session,
                                            aggregations={'value': ['sum', 'count']}).using(manager),),
        (VisualizeAggregatedCumsumTransfers(task_id='tf_agg_cumsum_log',
                                            session=session,
                                            aggregations={'value': ['sum', 'count']}, logy=True).using(manager),),

        (VisualizeAggregatedTransfersRolling(task_id='tf_agg_roll_khan-lee',
                                             session=session,
                                             aggregations={'value': ['sum', 'count']},
                                             window_size_days=21).using(manager),),
        (VisualizeAggregatedTransfersRolling(task_id='tf_agg_roll_window-60d',
                                             session=session,
                                             aggregations={'value': ['sum', 'count']},
                                             window_size_days=60).using(manager),),

        (VisualizeAggregatedTransfersRolling(task_id='tf_agg_roll_window-1d',
                                             session=session,
                                             aggregations={'value': ['sum', 'count']},
                                             window_size_days=1).using(manager),),

        (VisualizeAggregatedTransfersRolling(task_id='tf_agg_roll_window-1d_log',
                                             session=session,
                                             aggregations={'value': ['sum', 'count']}, logy=True,
                                             window_size_days=1).using(manager),),

        (VisualizeAggregatedTransfersRollingAutoCorrelation(task_id='tf_agg_roll_window-5d_ac',
                                                            session=session,
                                                            aggregations={'value': ['sum', 'count']}, logy=True,
                                                            window_size_days=5).using(manager),),

        (VisualizeHistogramTransferredValue(task_id='tf_hist_full_log', session=session, rwidth=0.8,
                                            logy=True, bins=100).using(manager),),

        (VisualizeHistogramTransferredValue(task_id='tf_hist_full_log_range-1.3e8',
                                            session=session,
                                            rwidth=0.8, logy=True, bins=100, boundaries=(0, 1.3e8)).using(manager),),

        (VisualizeHistogramTransferredValue(task_id='tf_hist_full_log_range-5e4',
                                            session=session,
                                            rwidth=0.8, logy=True, bins=100, boundaries=(0, 5e4)).using(manager),),

        (VisualizeHistogramTransferredValue(task_id='tf_hist_full_log_range-5e3',
                                            session=session,
                                            rwidth=0.8, logy=True, bins=100, boundaries=(0, 5e3)).using(manager),),

        (VisualizeHistogramTransferredValue(task_id='tf_hist_full_log_range-1e3',
                                            session=session,
                                            rwidth=0.8, logy=True, bins=100, boundaries=(0, 1e3)).using(manager),),

        (VisualizeHistogramTransferredValueRolling(task_id='tf_hist_rolling_log_khan-lee_window-21d7d',
                                                   session=session,
                                                   rwidth=0.8, logy=True, bins=100,
                                                   window_size_step_width_days=(21, 7)).using(manager),),

        (VisualizeHistogramTransferredValueRolling(task_id='tf_hist_rolling_log_window-60d21d',
                                                   session=session,
                                                   rwidth=0.8, logy=True, bins=100,
                                                   window_size_step_width_days=(60, 21)).using(manager),),

        (VisualizeHistogramTransferredValueRolling(task_id='tf_hist_rolling_log_window-60d21d_range-1.3e8',
                                                   session=session,
                                                   rwidth=0.8, logy=True, bins=100, boundaries=(0, 1.3e8),
                                                   window_size_step_width_days=(60, 21)).using(manager),),

        (VisualizeHistogramTransferredValueRolling(task_id='tf_hist_rolling_log_window-60d21d_range-5e4',
                                                   session=session,
                                                   rwidth=0.8, logy=True, bins=100, boundaries=(0, 5e4),
                                                   window_size_step_width_days=(60, 21)).using(manager),),

        (VisualizeHistogramTransferredValueRolling(task_id='tf_hist_rolling_log_window-60d21d_range-5e3',
                                                   session=session,
                                                   rwidth=0.8, logy=True, bins=100, boundaries=(0, 5e3),
                                                   window_size_step_width_days=(60, 21)).using(manager),),

        (VisualizeHistogramTransferredValueRolling(task_id='tf_hist_rolling_log_window-60d21d_range-1e3',
                                                   session=session,
                                                   rwidth=0.8, logy=True, bins=100, boundaries=(0, 1e3),
                                                   window_size_step_width_days=(60, 21)).using(manager),),

        (VisualizeAnimatedHistogramTransferredValueRolling(
            task_id='tf_anim_hist_log_khan-lee_window-21d7d_5fps',
            session=session,
            rwidth=0.8, logy=True, bins=100,
            window_size_step_width_days=(21, 7),
            fps=5).using(manager),),
        (VisualizeAnimatedHistogramTransferredValueRolling(
            task_id='tf_anim_hist_log_khan-lee_window-21d7d_5fps_range-1.3e8',
            session=session,
            rwidth=0.8, logy=True, bins=100, boundaries=(0, 1.3e8),
            window_size_step_width_days=(21, 7),
            fps=5).using(manager),),

        (VisualizeAnimatedHistogramTransferredValueRolling(
            task_id='tf_anim_hist_log_khan-lee_window-21d7d_5fps_range-5e4',
            session=session,
            rwidth=0.8, logy=True, bins=100, boundaries=(0, 5e4),
            window_size_step_width_days=(21, 7),
            fps=5).using(manager),),

        (VisualizeAnimatedHistogramTransferredValueRolling(
            task_id='tf_anim_hist_log_khan-lee_window-21d7d_5fps_range-5e3',
            session=session,
            rwidth=0.8, logy=True, bins=100, boundaries=(0, 5e3),
            window_size_step_width_days=(21, 7),
            fps=5).using(manager),),

        (VisualizeAnimatedHistogramTransferredValueRolling(
            task_id='tf_anim_hist_log_khan-lee_window-21d7d_5fps_range-1e3', session=session,
            rwidth=0.8, logy=True, bins=100, boundaries=(0, 1e3), window_size_step_width_days=(21, 7),
            fps=5).using(manager),),

        (VisualizeAnimatedHistogramTransferredValueRolling(
            task_id='tf_anim_hist_log_window-60d1d_21fps', session=session,
            rwidth=0.8, logy=True, bins=100, window_size_step_width_days=(60, 1),
            fps=21).using(manager),),

        (VisualizeAnimatedHistogramTransferredValueRolling(
            task_id='tf_anim_hist_log_window-60d1d_range-1.3e8_21fps', session=session,
            rwidth=0.8, logy=True, bins=100, boundaries=(0, 1.3e8), window_size_step_width_days=(60, 1),
            fps=21).using(manager),),

        (VisualizeAnimatedHistogramTransferredValueRolling(
            task_id='tf_anim_hist_log_window-60d1d_range-5e4_21fps', session=session,
            rwidth=0.8, logy=True, bins=100, boundaries=(0, 5e4), window_size_step_width_days=(60, 1),
            fps=21).using(manager),)
    ]
    results = p.starmap_async(executor, visualization_tasks)
    results.get()
    Task.print_track("[Main] Generation of visualizations", start_time)
    session.finished = True
    session.save()
    p.close()


if __name__ == '__main__':

    args = parse_args()

    if args.mode == 'start':
        print("[Main] Starting a new analysis session...")
        check_mode_start_args(args)
        session_config = SessionConfig(args=args)
        session_config.save()
        secrets_store = load_secrets(args)
    elif args.mode == 'resume':
        print("[Main] Resuming an analysis session...")
        session_config, secrets_store = recreate_session(args)
    else:
        raise AssertionError(f"Unhandled mode '{args.mode}'!")

    print("[Main] Current session data:")
    print(session_config)

    main(session_config, secrets_store)
