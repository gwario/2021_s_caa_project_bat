# 2021 S Caa Project Bat
Framework to analyse ethereum token transfer networks.

## Requirements
### System Level
At least 8 GB of RAM and a dual-core CPU is the minimum recommended system configuration.

Works satisfactory with 16 GB of RAM and a quad-core CPU. For animated histograms, 1 process is recommended due to high memory demand.

Tested with Windows 10 and debian bullseye. For Windows, you have to supply the path to ffmpeg.
```
python 3.9+
ffmpeg
```
Ffmpeg is necessary to render the animated histograms as mp4
### Python Packages
```requirements.txt
pandas==1.2.4
networkx==2.5.1
google-cloud-bigquery[bqstorage,pandas]==2.16.0
google-cloud-bigquery-storage==2.4.0
matplotlib==3.4.2
mplfinance==0.12.7a17
scipy==1.6.3
```
## How to Run
### Analyse a Token Transfer Network within a Block Range
```shell script
python analyse.py start --token-contract-address 0x3095a47305efd248f6ce272c2db01297a91e8c41 \
                        --token-name "Basic Attention Token" \
                        --token-ticker BAT \
                        --from-block-number 37500000 \
                        --until-block-number 47500000
```

### Resume a Previously Started Analysis Session
Resumes the most recent session in `./sessions` that has `finished: false`
```shell script
python analyse.py resume
```

### Modify the Analysis or its Parameters
`analyze.py` contains the different analysis pipelines.
You can increase the number of processes each pipeline can utilize.
You can add/remove tasks within a pipeline. The available tasks are in `utils/tasks`.
You can change parameters (logarithmic scale, ...) by changing the task parameters in a pipeline.

### Help / Usage
```shell script
$ python analyse.py -h
```
```
usage: analyse.py [-h] [--working-dir WORKING_DIR] [--data-dir DATA_DIR] [--reports-dir REPORTS_DIR] {resume,start} ...

Analyses an ethereum token (network).

positional arguments:
  {resume,start}        Modes of operation.
                        
    resume              In this mode, analyse.py either resume with a specific,
                        previously started session (--session), or the most recently
                        started session.
                        Type 'analyse.py -h resume' for more information.
                        
    start               In this mode, analyse.py starts a new analysis session.
                        Type 'analyse.py -h start' for more information.
                        

optional arguments:
  -h, --help            show this help message and exit
  --working-dir WORKING_DIR
                        The working directory path where session files are stored and 'secrets.json' file
                        is looked for.
                        Default: ./sessions
                        
                        Secrets file:   The 'secrets.json' file must contain api keys such.
                                        See sessions/example_secrets.json
                        
                        Session files:  A session file contains parameters and (intermediary) results.
                                        This file is created automatically when an analysis has been started.
                                        E.g. session_<token-ticker>_<timestamp_ns>.json
  --data-dir DATA_DIR   The data directory path, where data is read from and saved to.
                        Default: ./data
  --reports-dir REPORTS_DIR
                        The reports directory path, where results will be saved to.
                        Default: ./reports
```

