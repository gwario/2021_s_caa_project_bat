#!/bin/sh

python analyse.py start --token-contract-address 0x3095a47305efd248f6ce272c2db01297a91e8c41 \
                        --token-name "Basic Attention Token" \
                        --token-ticker BAT \
                        --from-block-number 37500000 \
                        --until-block-number 47500000