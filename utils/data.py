import decimal
import json
import pathlib
from argparse import Namespace
from typing import Union, Dict

import pandas
from pandas import DataFrame

from utils.patterns import re_token_prices_file, re_token_transfers_file, re_session_config_file
from utils.session_config import SessionConfig


def apply_decimal_correction_to_token_transfers_value(df_token_transfers: DataFrame, df_token: DataFrame) -> DataFrame:
    """
    Corrects the values of a given token transfers data frame according to the number of decimal of the token.
    I.e. performs a division by 10^decimals
    :param df_token_transfers:
    :param df_token:
    :return: return corrected version of the given transfers data frame
    """
    decimal_count = df_token.loc[0, 'decimals']
    dec_ctx = decimal.getcontext()
    df_token_transfers['value'] = df_token_transfers['value'] \
        .apply(lambda value: dec_ctx.multiply(value, dec_ctx.power(10, -int(decimal_count))))
    # print(df_token_transfers)
    return df_token_transfers


def load_secrets(args: Namespace) -> Dict:
    """
    Loads the secrets from <working-dir>/secrets.json
    :param args:
    :return: the secrets.
    """
    return json.loads((args.working_dir / 'secrets.json').read_text())


def load_token_transfer_data(data_dir: pathlib.Path,
                             token_contract_address: str, from_block_number: int, until_block_number: int
                             ) -> DataFrame:
    """
    Loads the token transfer data file.
    :param data_dir:
    :param token_contract_address:
    :param from_block_number:
    :param until_block_number:
    :raise FileNotFoundError: if there is none matching the token and block range
    :return: the token transfer as data frame.
    """
    token_transfers_file = get_token_transfer_data_file(data_dir, token_contract_address, from_block_number,
                                                        until_block_number)

    if token_transfers_file:
        fp = token_transfers_file.open()
        df_token_transfers = pandas.read_csv(fp, converters={'value': lambda val: decimal.Decimal(val)})
        fp.close()
        df_token_transfers = df_token_transfers.convert_dtypes()
        # df_token_transfers['value'] = df_token_transfers['value'].astype(float)
        df_token_transfers.drop(df_token_transfers.columns[[0]], axis=1, inplace=True)
        df_token_transfers.set_index(['block_number', 'block_timestamp'], inplace=True)
        return df_token_transfers
    else:
        raise FileNotFoundError('No transfers file found that matches the requirements!')


def get_token_transfer_data_file(data_dir: pathlib.Path, token_contract_address: str, from_block_number: int,
                                 until_block_number: int) -> Union[pathlib.Path, None]:
    """
    :param data_dir:
    :param token_contract_address:
    :param from_block_number:
    :param until_block_number:
    :return: The token transfer data file or none if there is none matching the token and block range.
    """
    token_transfer_files = [
        node
        for node in data_dir.iterdir()
        if node.is_file() and re_token_transfers_file.match(node.name)
    ]
    satisfactory_token_transfer_files = filter(lambda node:
                                               _transfers_file_name_satisfies_addr_from_until(
                                                   node.name,
                                                   token_contract_address, from_block_number, until_block_number
                                               ),
                                               token_transfer_files)
    satisfactory_token_transfer_file_list = list(satisfactory_token_transfer_files)
    if len(satisfactory_token_transfer_file_list) > 0:
        return satisfactory_token_transfer_file_list[0]
    else:
        return None


def load_token_data(data_dir: pathlib.Path, token_contract_address: str) -> DataFrame:
    """
    Loads the token data file.
    :param data_dir:
    :param token_contract_address:
    :raise FileNotFoundError: if there is none matching the token
    :return: the token as data frame.
    """
    token_file = get_token_data_file(data_dir, token_contract_address)

    if token_file:
        fp = token_file.open()
        df_token = pandas.read_csv(fp)
        fp.close()
        df_token = df_token.convert_dtypes()
        df_token.drop(df_token.columns[[0]], axis=1, inplace=True)
        return df_token
    else:
        raise FileNotFoundError('No token file found that matches the requirements!')


def get_token_data_file(data_dir: pathlib.Path, token_contract_address: str) -> Union[pathlib.Path, None]:
    """
    :param data_dir:
    :param token_contract_address:
    :return: The token data file or none if there is none matching the token.
    """
    token_file = data_dir / f'tokens_{token_contract_address}.csv'

    if token_file.is_file():
        return token_file
    else:
        return None


def load_token_prices_data(data_dir: pathlib.Path, token_contract_address: str,
                           from_block_number: int, until_block_number: int) -> DataFrame:
    """
    Loads the token price data file.
    :param data_dir:
    :param token_contract_address:
    :param from_block_number:
    :param until_block_number:
    :raise FileNotFoundError: if there is none matching the token
    :return: the prices as data frame.
    """
    prices_file = get_token_prices_data_file(data_dir, token_contract_address, from_block_number, until_block_number)

    if prices_file:
        fp = prices_file.open()
        df_prices = pandas.read_csv(fp, index_col=0, header=[0, 1])
        fp.close()
        df_prices = df_prices.convert_dtypes()
        return df_prices
    else:
        raise FileNotFoundError('No prices file found that matches the requirements!')


def get_token_prices_data_file(data_dir: pathlib.Path, token_contract_address: str, from_block_number: int,
                               until_block_number: int) -> Union[pathlib.Path, None]:
    """
    :param data_dir:
    :param token_contract_address:
    :param from_block_number:
    :param until_block_number:
    :return: The token prices data file or none if there is none matching the token and block range.
    """
    token_prices_files = [
        node
        for node in data_dir.iterdir()
        if node.is_file() and re_token_prices_file.match(node.name)
    ]
    satisfactory_token_prices_files = filter(lambda node:
                                             _prices_file_name_satisfies_addr_from_until(
                                                 node.name,
                                                 token_contract_address, from_block_number, until_block_number
                                             ),
                                             token_prices_files)
    satisfactory_token_prices_file_list = list(satisfactory_token_prices_files)
    if len(satisfactory_token_prices_file_list) > 0:
        return satisfactory_token_prices_file_list[0]
    else:
        return None


def _transfers_file_name_satisfies_addr_from_until(file_name: str, required_addr, required_from,
                                                   required_until) -> bool:
    """
    Checks whether the file name's indicated data matches the required data.
    :param file_name:
    :param required_addr:
    :param required_from:
    :param required_until:
    :return: true if the addresses match
     and the required interval [from, until] is in the from-until-interval from the file name.
    """
    match = re_token_transfers_file.match(file_name)
    return match.group('addr') == required_addr\
        and int(match.group('from')) <= required_from and int(match.group('until')) >= required_until


def _prices_file_name_satisfies_addr_from_until(file_name: str, required_addr, required_from, required_until) -> bool:
    """
    Checks whether the file name's indicated data matches the required data.
    :param file_name:
    :param required_addr:
    :param required_from:
    :param required_until:
    :return: true if the addresses match
     and the required interval [from, until] is in the from-until-interval from the file name.
    """
    match = re_token_prices_file.match(file_name)
    return match.group('addr') == required_addr\
        and int(match.group('from')) <= required_from and int(match.group('until')) >= required_until


def token_transfers_data_exists(session_config):
    """
    :param session_config:
    :return: true if there is a file containing the transfers for the token and block range.
    """
    satisfactory_token_transfer_file = get_token_transfer_data_file(session_config.args['data_dir'],
                                                                    session_config.args['token_contract_address'],
                                                                    session_config.args['from_block_number'],
                                                                    session_config.args['until_block_number'])
    return True if satisfactory_token_transfer_file else False


def token_prices_data_exists(session_config):
    """
    :param session_config:
    :return: true if there is a file containing the prices for the token and block range.
    """
    satisfactory_token_prices_file = get_token_prices_data_file(session_config.args['data_dir'],
                                                                session_config.args['token_contract_address'],
                                                                session_config.args['from_block_number'],
                                                                session_config.args['until_block_number'])
    return True if satisfactory_token_prices_file else False


def load_most_recent_incomplete_session(args: Namespace) -> SessionConfig:
    """
    :param args: the args to get the directory to look for sessions
    :return: The most recent incomplete (finished == False) session configuration.
    """
    # look for files of existing sessions
    session_nodes = [
        node
        for node in args.working_dir.iterdir()
        if node.is_file() and re_session_config_file.match(node.name)
    ]
    # load previous sessions
    session_configs = map(lambda node: SessionConfig(existing_session=json.loads(node.read_text()),
                                                     working_dir=args.working_dir, data_dir=args.data_dir,
                                                     reports_dir=args.reports_dir), session_nodes)
    # find incomplete sessions
    incomplete_session_configs = list(filter(lambda session_config: not session_config.finished, session_configs))
    if len(incomplete_session_configs) == 0:
        raise FileNotFoundError('No incomplete(!) sessions found!')
    # return the most recent one
    most_recent_incomplete_session = max(incomplete_session_configs,
                                         key=lambda session_config: session_config.timestamp.timestamp())
    return most_recent_incomplete_session
