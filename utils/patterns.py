import re

block_number_pattern = r"\d+"
token_ticker_pattern = r'[a-zA-Z0-9]+'
token_contract_address_pattern = r'0x[a-f0-9]{40}'

# modified version from https://stackoverflow.com/a/3143231
iso_timestamp_pattern = r'\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+'
unix_timestamp_pattern = r'\d+'


re_token_transfers_file = re.compile(fr"transfers_(?P<addr>{token_contract_address_pattern})_(?P<from>{block_number_pattern})_(?P<until>{block_number_pattern})\.csv")
re_token_prices_file = re.compile(fr"prices_(?P<addr>{token_contract_address_pattern})_(?P<from>{block_number_pattern})_(?P<until>{block_number_pattern})\.csv")
re_session_config_file = re.compile(rf'session_{token_ticker_pattern}_{unix_timestamp_pattern}\.json')
