import datetime
import json
from multiprocessing import Manager
from pprint import pformat


class SessionConfig:
    """Representation of an analysis session."""

    def __init__(self, **kwargs):
        """
        :param args: (args: Namespace, tasks: List[str])
         or (session_config: Dict, working_dir: pathlib.Path, data_dir: pathlib.Path, reports_dir: pathlib.Path)
        """
        if kwargs.get('args'):
            # create new session based on commandline args
            self.args = kwargs.get('args').__dict__
            # remove mode
            del self.args['mode']
            self.timestamp = datetime.datetime.utcnow()
            self.finished = False
            # create structure of available tasks
            self.progress = {}

        elif kwargs.get('existing_session'):
            existing_session = kwargs.get('existing_session')
            # create from previous session (from json) and supplied paths
            self.args = existing_session.get('args')
            # add supplied directories
            self.args['working_dir'] = kwargs.get('working_dir')
            self.args['data_dir'] = kwargs.get('data_dir')
            self.args['reports_dir'] = kwargs.get('reports_dir')
            self.timestamp = datetime.datetime.fromisoformat(existing_session.get('timestamp'))
            self.finished = existing_session.get('finished')
            self.progress = existing_session.get('progress')

        self.manager = None
        self.task_template = {
            "completed": False
        }

    def __str__(self):
        # return f'{self.__class__}({self.timestamp},{self.progress},{self.finished})'
        members_to_print = {
            'timestamp': self.timestamp,
            'progress': self.progress,
            'finished': self.finished,
            'args': self.args}
        return f'{type(self).__name__}(\n{pformat(members_to_print, indent=4)}\n)'

    def update_progress_task_complete(self, task_key):
        if not self.progress.get(task_key):
            self.init_task(task_key)
        self.progress[task_key]['completed'] = True
        self.save()

    def has_task_been_completed(self, task_key) -> bool:
        if not self.progress.get(task_key):
            self.init_task(task_key)
        return self.progress.get(task_key)['completed']

    def save(self):
        args = self.args.copy()
        del args['working_dir']
        del args['data_dir']
        del args['reports_dir']
        obj_to_store = {
            'timestamp': self.timestamp.isoformat(),
            'progress': dict({k: dict(v) for k, v in self.progress.items()}),
            'finished': self.finished,
            'args': dict(args)
        }
        serialized_obj_to_store = json.dumps(obj_to_store, indent=2, sort_keys=True)
        unix_timestamp_s = datetime.datetime.timestamp(self.timestamp)
        unix_timestamp_ns = int(unix_timestamp_s * 1000 * 1000)
        sessions_file_name = f'session_{self.args["token_ticker"]}_{unix_timestamp_ns}.json'
        (self.args['working_dir'] / sessions_file_name).write_text(serialized_obj_to_store)

    def use(self, manager: Manager):
        self.args = manager.dict(self.args)
        self.progress = manager.dict({task_name: manager.dict(progress_dict)
                                      for task_name, progress_dict in self.progress.items()})

    def init_task(self, task_key: str, manager: Manager = None):
        if manager:
            self.progress[task_key] = manager.dict(self.task_template.copy())
        else:
            self.progress[task_key] = self.task_template.copy()
        self.save()
