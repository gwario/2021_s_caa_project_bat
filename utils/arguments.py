import json
import pathlib
import textwrap
from argparse import Namespace, ArgumentParser, RawTextHelpFormatter
from typing import Dict, Tuple, Optional

from utils.data import token_transfers_data_exists, token_prices_data_exists, load_secrets, \
    load_most_recent_incomplete_session
from utils.patterns import token_contract_address_pattern, token_ticker_pattern
from utils.session_config import SessionConfig
from utils.validation import validate_secrets, validate_token_arguments

default_data_dir = pathlib.Path('./data')
default_reports_dir = pathlib.Path('./reports')
default_working_dir = pathlib.Path('./sessions')


def parse_args() -> Namespace:
    """
    Parses the commandline arguments and returns them.
    :return: the arguments
    """
    parser = ArgumentParser(description='Analyses an ethereum token (network).', formatter_class=RawTextHelpFormatter)
    parser.add_argument('--working-dir', type=pathlib.Path, required=False, default=default_working_dir,
                        help=textwrap.dedent('''\
                        The working directory path where session files are stored and 'secrets.json' file
                        is looked for.
                        Default: ./sessions

                        Secrets file:   The 'secrets.json' file must contain api keys such.
                                        See sessions/example_secrets.json

                        Session files:  A session file contains parameters and (intermediary) results.
                                        This file is created automatically when an analysis has been started.
                                        E.g. session_<token-ticker>_<timestamp_ns>.json
                        '''))
    parser.add_argument('--data-dir', type=pathlib.Path, required=False, default='./data', help=textwrap.dedent('''\
        The data directory path, where data is read from and saved to.
        Default: ./data
        '''))
    parser.add_argument('--reports-dir', type=pathlib.Path, required=False, default='./reports',
                        help=textwrap.dedent('''\
                        The reports directory path, where results will be saved to.
                        Default: ./reports
                        '''))
    subparsers = parser.add_subparsers(dest='mode', help=textwrap.dedent('''\
        Modes of operation.

        '''))

    resume_parser = subparsers.add_parser('resume', formatter_class=RawTextHelpFormatter, help=textwrap.dedent('''\
        In this mode, %(prog)s either resume with a specific,
        previously started session (--session), or the most recently
        started session.
        Type '%(prog)s -h resume' for more information.

        '''))
    resume_parser.add_argument('--session', type=str, required=False, help=textwrap.dedent('''\
        A session to resume. E.g. 'session_<token-ticker>_<timestamp>'
        If not specified, the most recent session_<token-ticker>_<timestamp_ns>.json
        from the working directory will be used.
        '''))

    start_parser = subparsers.add_parser('start', formatter_class=RawTextHelpFormatter, help=textwrap.dedent('''\
        In this mode, %(prog)s starts a new analysis session.
        Type '%(prog)s -h start' for more information.

        '''))
    start_parser.add_argument('--token-contract-address', type=str, required=True, help=textwrap.dedent(f'''\
        The contract address ({token_contract_address_pattern}) of the token to be analysed.
        E.g. 0x0d8775f648430679a709e98d2b0cb6250d2887ef
        '''))
    start_parser.add_argument('--token-name', type=str, required=True,
                              help='The full-length name of the token. Used for labels.')
    start_parser.add_argument('--token-ticker', type=str, required=True,
                              help=f'The token`s ticker/slug/abbreviation ({token_ticker_pattern}). Used for labels.')
    start_parser.add_argument('--from-block-number', type=int, required=True,
                              help='The block number from (inclusive) which the analysis should be performed.')
    start_parser.add_argument('--until-block-number', type=int, required=True,
                              help='The block number until (inclusive) which the analysis should be performed.')

    return parser.parse_args()


def check_mode_start_args(args: Namespace):
    """
    Basic checks to validate argument values for the start mode.
    :param args: the commandline arguments
    """
    # check args for mode start
    validate_secrets(args)
    validate_token_arguments(args)


def recreate_session(args: Namespace) -> Tuple[SessionConfig, Optional[Dict]]:
    """
    Recreates a session. Performs checks to validate argument values for the resume mode and loads the secrets if
    necessary.
    :param args: the commandline arguments
    :return The session config and secrets.
    """
    # check args for mode resume
    if args.session:
        session_config = SessionConfig(existing_session=json.loads((args.working_dir / f'{args.session}.json').read_text()),
                                       working_dir=args.working_dir, data_dir=args.data_dir,
                                       reports_dir=args.reports_dir)
        if session_config.finished:
            raise AssertionError('The provided sessions has already been finished!')
    else:
        if not args.working_dir.is_dir():
            raise NotADirectoryError()

        session_config = load_most_recent_incomplete_session(args)

        if not session_config:
            raise AssertionError('There is no session to resume!')

        validate_token_arguments(Namespace(**session_config.args))

    # the session config is ready at this point

    if token_transfers_data_exists(session_config) and token_prices_data_exists(session_config):
        # no secrets required
        secrets = None
    else:
        validate_secrets(args)
        secrets = load_secrets(args)

    return session_config, secrets
