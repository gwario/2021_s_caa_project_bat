import re
from argparse import Namespace

from utils.data import load_secrets
from utils.patterns import token_contract_address_pattern, token_ticker_pattern


def validate_token_arguments(args: Namespace):
    """
    Checks whether
     - from-block-number >= 0
     - from-block-number <= until-block-number
     - token-contract-address matches 0x[a-f0-9]{40}
     - token-name is not blank
     - token-ticker is not blank and does not contain spaces
    :param args: the arguments
    """
    if not args.token_name or not args.token_name.strip():
        raise ValueError(f'Invalid token name \'{args.token_name}\'!')

    if not args.token_ticker or not re.match(f'^{token_ticker_pattern}$', args.token_ticker):
        raise ValueError(f'Invalid token ticker \'{args.token_ticker}\'!')

    if not args.token_contract_address or not re.match(f'^{token_contract_address_pattern}$', args.token_contract_address):
        raise ValueError(f'Invalid token contract address \'{args.token_contract_address}\'!')

    if not args.from_block_number >= 0:
        raise ValueError(f'From block number must be >= 0!')

    if not args.until_block_number >= args.from_block_number:
        raise ValueError(f'Until block number must be >= from block number!')


def validate_secrets(args: Namespace):
    """
    Checks whether the secrets for collecting the data are provided
    :return:
    """
    secrets_file = args.working_dir / 'secrets.json'
    if not secrets_file.is_file():
        raise ValueError(f'Secrets not found! {args.working_dir}/secrets.json')

    secrets = load_secrets(args)

    crypto_compare_api_key = secrets.get('crypto_compare', {}).get('api_key')
    if not crypto_compare_api_key:
        raise ValueError('Secrets is missing crypto_compare.api_key!')
