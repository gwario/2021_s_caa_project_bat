import time

import networkx
import pandas

from utils.session_config import SessionConfig
from utils.task import Task


class CalculateTokenNetGraphCharacteristics(Task):
    """
    Calculates basic graph characteristics on the full transfer graph.
    Metrics:
     - Number of nodes (unique addresses)
     - In the directed multi graph
       - Number of edges (transfers)
       - Number of self-loops (transfers)
       - Density
     - In the undirected simple graph
       - Number of edges (transfers)
       - Number of self-loops (transfers)
       - Density
    Requires files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_mdg.gpickle
    Created files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_gm_full.csv
    """

    def __init__(self, session: SessionConfig):
        f"""
        Will be available in self
        :param session:
        """
        super().__init__(session=session)

    def do_work(self):
        """
        Loads the transfers graph and calculates basic characteristics. Stores the resulting data as csv.
        transfers_<token_contract_address>_<from_block_number>_<until_block_number>_gm_full.csv
        """
        data_dir = self.session.args['data_dir']
        token_contr_addr = self.session.args['token_contract_address']
        from_blk_nr = self.session.args['from_block_number']
        until_blk_nr = self.session.args['until_block_number']

        start_time = time.perf_counter()

        g_orig = networkx.read_gpickle(data_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_mdg.gpickle")
        start_time = Task.print_track("Loading the graph", start_time)

        # extract relevant time frame by block number
        # filter edges in block range
        def block_range_filter(s, t, k):
            return from_blk_nr <= g_orig.edges[s, t, k]['block_number'] <= until_blk_nr

        g_edge_restricted = networkx.subgraph_view(g_orig, filter_edge=block_range_filter)
        nodes = set()
        for u, v in g_edge_restricted.edges():
            nodes.add(u)
            nodes.add(v)
        # get view of graph with edges and nodes removed
        g_restricted = networkx.subgraph_view(g_orig, filter_node=lambda n: n in nodes, filter_edge=block_range_filter)
        start_time = Task.print_track("Filtering the target block range", start_time)

        num_nodes = networkx.number_of_nodes(g_restricted)
        start_time = Task.print_track("Calculation of the number of nodes", start_time)

        # directed multi graph
        num_edges_mdg = networkx.number_of_edges(g_restricted)
        start_time = Task.print_track("Calculation of the number of edges on the MDG", start_time)

        num_sls_mdg = networkx.number_of_selfloops(g_restricted)
        start_time = Task.print_track("Calculation of the number of self-loops on the MDG", start_time)

        density_mdg = networkx.density(g_restricted)
        start_time = Task.print_track("Calculation of the density on the MDG", start_time)

        # simple undirected graph
        g = networkx.Graph(g_restricted)
        del g_restricted
        start_time = Task.print_track("Creation of the G", start_time)

        num_edges_g = networkx.number_of_edges(g)
        start_time = Task.print_track("Calculation of the number of edges on the G", start_time)

        num_sls_g = networkx.number_of_selfloops(g)
        start_time = Task.print_track("Calculation of the number of self-loops on the G", start_time)

        density_g = networkx.density(g)  # TODO density is higher for g than for mdg... seems wrong
        Task.print_track("Calculation of the density on the G", start_time)

        df = pandas.DataFrame(columns=[
            'nodes',
            'edges (Graph)', 'self-loops (Graph)', 'density (Graph)',
            'edges (MultiDiGraph)', 'self-loops (MultiDiGraph)', 'density (MultiDiGraph)'])
        df.loc[1, :] = (
            num_nodes,
            num_edges_g, num_sls_g, density_g,
            num_edges_mdg, num_sls_mdg, density_mdg)
        df.to_csv(data_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_gm_full.csv")
