from typing import List, Dict, Union

import pandas

from utils.data import load_token_transfer_data, apply_decimal_correction_to_token_transfers_value, load_token_data
from utils.session_config import SessionConfig
from utils.task import Task


class CalculateAggregatedTransfersRolling(Task):
    """
    Aggregates transfers over a number of days rolling.
    This task can be used multiple times when a unique task_id is provided.
    NOTE: the result is as described in https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.aggregate.html
    when passing a dictionary to aggregate! i.e. functions on the row index and column names on the column index.
    Requires files:
     - token_<token_contract_address>.csv
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>.csv
    Created files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_{col}-{agg1+agg2+...}_roll-{window_size}d{_task_id}.csv
     NOTE: To control the overlap with window size l, select every n-th row to get a step width of n. if n<l,
      the window has an overlap.
    """

    def __init__(self, session: SessionConfig, task_id: str, aggregations: Dict[str, Union[List[str], str]],
                 window_size_days: int):
        f"""
        Will be available in self
        :param session:
        :param task_id:
        :param aggregations: A dictionary of aggregations. Keys are the column names and values are the list of
        aggregation functions to be performed on the column.
        See https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.aggregate.html
        :param window_size_days: the window size in days.
        """
        super().__init__(session=session, task_id=task_id, aggregations=aggregations, window_size_days=window_size_days)

    def do_work(self):
        """
        Loads the transfers data and groups and aggregates. Stores the resulting data as csv.
        transfers_<token_contract_address>_<from_block_number>_<until_block_number>_{col}-{agg1+agg2+...}_roll-{window_size}d{_task_id}.csv
        """
        data_dir = self.session.args['data_dir']
        token_contr_addr = self.session.args['token_contract_address']
        from_blk_nr = self.session.args['from_block_number']
        until_blk_nr = self.session.args['until_block_number']

        df_token = load_token_data(data_dir, token_contr_addr)
        df_token_transfers = load_token_transfer_data(data_dir, token_contr_addr, from_blk_nr, until_blk_nr)

        df_token_transfers = apply_decimal_correction_to_token_transfers_value(df_token_transfers, df_token)

        df_token_transfers = df_token_transfers.loc[(from_blk_nr,):(until_blk_nr,), :]

        df_token_transfers.index = df_token_transfers.index.droplevel('block_number')
        df_token_transfers.index = pandas.to_datetime(df_token_transfers.index.to_series())

        # generate the column multi-index that will be created by this aggregate
        agg_columns = list()
        for (col, agg_s) in self.aggregations.items():
            if type(agg_s) is list:
                agg_columns.extend([(col, agg) for agg in agg_s])
            else:
                agg_columns.append((col, agg_s))
        # group by + aggregate by day: a day will be the smallest step width and the only permissible unit for window width
        df_token_transfers_day_agg = df_token_transfers \
            .groupby(pandas.Grouper(level='block_timestamp', freq='D')) \
            .aggregate(self.aggregations)
        # (center is not supported for datetime like and offset)
        # from now on only sums are permissible!
        # the window/interval is closed on the right side, it always ends at/with the the specific day (the upper bound)
        # see https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.rolling.html for rolling function params
        # see https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#dateoffset-objects for window offsets
        # see https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.core.window.rolling.Rolling.aggregate.html for aggregations
        # print(df_token_transfers_day_agg.columns)
        df_token_transfers_day_agg_rolling = df_token_transfers_day_agg \
            .rolling(window=pandas.offsets.Day(self.window_size_days)) \
            .aggregate({col: "sum" for col in agg_columns})

        df_token_transfers_day_agg_rolling.info()
        # print(df_token_transfers_day_agg_rolling)
        for col_name, col_agg_s in self.aggregations.items():
            aggs_str = '+'.join(col_agg_s) if type(col_agg_s) is list else col_agg_s
            # get all indices for the given column name
            col_index = [(c_n, col_agg) for (c_n, col_agg) in agg_columns if c_n == col_name]
            df_token_transfers_day_agg_rolling.loc[:, col_index].to_csv(
                data_dir / f'transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_{col_name}-{aggs_str}_roll-{self.window_size_days}d{self.get_task_suffix()}.csv'
            )
