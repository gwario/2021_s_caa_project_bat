import time
from typing import Optional, Tuple

import pandas
from matplotlib import pyplot as plt
from matplotlib.dates import DateFormatter, YearLocator, MonthLocator

from utils.session_config import SessionConfig
from utils.task import Task


class VisualizeCoreDecompositionRolling(Task):
    """
    Visualizes the core decomposition of the transfer graph.
    Requires files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_cd_roll-<window_size_days>d-<shift_size_days>d.csv
    Created files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_cd_roll-<window_size_days>d-<shift_size_days>d_<col_name>.png
    """

    def __init__(self, session: SessionConfig, window_size_step_width_days: Tuple[int, Optional[int]],
                 logy: bool = False):
        f"""
        Will be available in self
        :param session:
        :param logy: Logarithmic scale on the y if true
        :param window_size_step_width_days: tuple of window size and step width in days; if step width is none, non
         overlapping windows; if None for whole time span
        See https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.html
        """
        super().__init__(session=session, logy=logy, window_size_step_width_days=window_size_step_width_days)

    def do_work(self):
        """
        Creates a line plot of the aggregated of transfers.
        """
        data_dir = self.session.args['data_dir']
        reports_dir = self.session.args['reports_dir']
        token_contr_addr = self.session.args['token_contract_address']
        from_blk_nr = self.session.args['from_block_number']
        until_blk_nr = self.session.args['until_block_number']
        step_width_days, window_size_days = VisualizeCoreDecompositionRolling.get_window_step_params(
            self.window_size_step_width_days)

        start_time = time.perf_counter()

        df = pandas.read_csv(
            data_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_cd_roll-{window_size_days}d-{step_width_days}d.csv",
            index_col=0)
        df.index = pandas.to_datetime(df.index.to_series())
        Task.print_track(f"Loading core decomposition data", start_time)
        df.info()

        px = 1 / plt.rcParams['figure.dpi']
        fig_size = (1920 * px, 1080 * px)

        for col in df.columns:
            fig, ax = plt.subplots(figsize=fig_size)
            df.loc[:, col].plot(xlabel='Time', ylabel=col, logy=self.logy, ax=ax)

            ax.set_xticklabels(ax.get_xticks(), rotation=90)
            ax.xaxis.set_major_formatter(DateFormatter("%Y-%m"))
            ax.xaxis.set_major_locator(YearLocator())
            ax.xaxis.set_minor_formatter(DateFormatter("%m"))
            ax.xaxis.set_minor_locator(MonthLocator())
            plt.grid(b=True, which='both', axis='both', linestyle='-')
            fig.suptitle('Temporal Progress of Core Decomposition')
            fig.tight_layout()
            col_file_suffix = "".join(c for c in col if c.isalnum() or c in "_")
            fig.savefig(reports_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_cd_roll-{window_size_days}d-{step_width_days}d_{col_file_suffix}.png")

        plt.close()

    @staticmethod
    def get_window_step_params(window_size_days_step_width_days):
        if window_size_days_step_width_days:
            window_size_days, step_width_days = window_size_days_step_width_days
            if not step_width_days:
                step_width_days = window_size_days
        else:
            window_size_days = None
            step_width_days = None
        return step_width_days, window_size_days
