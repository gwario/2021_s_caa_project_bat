import time
from typing import Optional, Tuple, List

import pandas
from matplotlib import pyplot as plt
from matplotlib.dates import DateFormatter, YearLocator, MonthLocator

from utils.data import load_token_prices_data
from utils.session_config import SessionConfig
from utils.task import Task


class VisualizeCoreDecompositionRollingAndPrice(Task):
    """
    Visualizes the core decomposition of the transfer graph and token price as time series diagram and their correlation(s).
    Requires files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_cd_roll-<window_size_days>d-<shift_size_days>d.csv
    Created files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_cd_roll-<window_size_days>d-<shift_size_days>d_<col_name>.png
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_cd_roll-<window_size_days>d-<shift_size_days>d_corr-<method>.csv
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_cd_roll-<window_size_days>d-<shift_size_days>d_diffcorr-<method>.csv
    """

    def __init__(self, session: SessionConfig, window_size_step_width_days: Tuple[int, Optional[int]],
                 price_pair: Tuple[str, str] = None, corr_methods: List[str] = ['pearson', 'kendall', 'spearman']):
        f"""
        Will be available in self
        :param session:
        :param price_pair: must be(!): (token_you_analyse, other_currency)
        :param window_size_step_width_days: tuple of window size and step width in days; if step width is none, non
         overlapping windows; if None for whole time span
        :param corr_methods: correlations to calculate.
        See https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.html
        See https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.corr.html
        See https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.diff.html
        """
        super().__init__(session=session, price_pair=price_pair, corr_methods=corr_methods,
                         window_size_step_width_days=window_size_step_width_days)

    def do_work(self):
        """
        Creates a line plot of the aggregated of transfers.
        """
        data_dir = self.session.args['data_dir']
        reports_dir = self.session.args['reports_dir']
        token_contr_addr = self.session.args['token_contract_address']
        token_ticker = self.session.args['token_ticker']
        from_blk_nr = self.session.args['from_block_number']
        until_blk_nr = self.session.args['until_block_number']
        step_width_days, window_size_days = VisualizeCoreDecompositionRollingAndPrice.get_window_step_params(
            self.window_size_step_width_days)

        if not self.price_pair:
            self.price_pair = (token_ticker, 'USD')

        start_time = time.perf_counter()

        df_core_decomposition = pandas.read_csv(
            data_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_cd_roll-{window_size_days}d-{step_width_days}d.csv",
            index_col=0)
        df_core_decomposition.index = pandas.to_datetime(df_core_decomposition.index.to_series())
        start_time = Task.print_track(f"Loading core decomposition data", start_time)

        df_token_prices = load_token_prices_data(data_dir, token_contr_addr, from_blk_nr, until_blk_nr)
        df_token_prices.index = pandas.to_datetime(df_token_prices.index.to_series())
        df_token_prices = df_token_prices.drop(
            ['high', 'low', 'volumeto', 'volumefrom', 'conversionSymbol', 'conversionType'], axis=1, level=1)
        price_pair_column = "-".join(self.price_pair)
        df_token_prices = df_token_prices.loc[
                          df_core_decomposition.index.min():df_core_decomposition.index.max(), (price_pair_column,)]
        # high and low to kind of get the peaks... could be overlooked for more than a day when using open and close
        # open and close to avoid weird things in the plots
        ylabel_price = f'Price in {self.price_pair[1]}'
        df_token_prices.loc[:, ylabel_price] = df_token_prices.loc[:, ['open', 'close']].mean(axis=1)
        df_token_prices = df_token_prices.drop(['open', 'close'], axis=1)
        start_time = Task.print_track(f"Loading token price data", start_time)

        px = 1 / plt.rcParams['figure.dpi']
        fig_size = (1920 * px, 800 * px)
        pandas.options.display.max_columns = None

        price_file_suffix = self.price_pair[1]

        df_joined = df_token_prices.join(df_core_decomposition, how='outer')
        for corr_method in self.corr_methods:
            df_joined.corr(method=corr_method).to_csv(
                reports_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_cd_roll-{window_size_days}d-{step_width_days}d_{price_file_suffix}_corr-{corr_method}.csv"
            )
        df_joined = df_joined.dropna()
        for corr_method in self.corr_methods:
            df_joined.diff().corr(method=corr_method).to_csv(
                reports_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_cd_roll-{window_size_days}d-{step_width_days}d_{price_file_suffix}_diffcorr-{corr_method}.csv"
            )
        del df_joined
        Task.print_track(f"Calculation of core decomposition metrics and price data", start_time)

        for col in df_core_decomposition.columns:
            fig, ax = plt.subplots(figsize=fig_size)
            ax_prim = df_token_prices.loc[:, ylabel_price].plot(xlabel='Time', ax=ax)
            ax_prim.set_ylabel(ylabel_price)
            ax_sec = df_core_decomposition.loc[:, col].plot(xlabel='Time', secondary_y=True, ax=ax_prim)
            ax_sec.set_ylabel(col)

            ax.legend([ax_prim.get_lines()[0], ax_sec.get_lines()[0]], [ylabel_price, col])
            ax.set_xticklabels(ax.get_xticks(), rotation=90)
            ax.xaxis.set_major_formatter(DateFormatter("%Y-%m"))
            ax.xaxis.set_major_locator(YearLocator())
            ax.xaxis.set_minor_formatter(DateFormatter("%m"))
            ax.xaxis.set_minor_locator(MonthLocator())

            ax_prim.grid(b=True, which='both', axis='x', linestyle='-')
            ax_sec.grid(b=True, which='both', axis='y', linestyle='-')

            fig.suptitle('Temporal Progress of Core Decomposition')

            fig.tight_layout()
            col_file_suffix = "".join(c for c in col if c.isalnum() or c in "_")
            fig.savefig(
                reports_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_cd_roll-{window_size_days}d-{step_width_days}d_{col_file_suffix}-{price_file_suffix}.png")

        plt.close()

    @staticmethod
    def get_window_step_params(window_size_days_step_width_days):
        if window_size_days_step_width_days:
            window_size_days, step_width_days = window_size_days_step_width_days
            if not step_width_days:
                step_width_days = window_size_days
        else:
            window_size_days = None
            step_width_days = None
        return step_width_days, window_size_days
