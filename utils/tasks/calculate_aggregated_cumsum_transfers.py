from typing import List, Dict, Union

import pandas

from utils.data import load_token_transfer_data, load_token_data, apply_decimal_correction_to_token_transfers_value
from utils.session_config import SessionConfig
from utils.task import Task


class CalculateAggregatedCumsumTransfers(Task):
    """
    Aggregates transfers per day and calculates the cumulative sum.
    NOTE: the result is as described in https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.aggregate.html
    when passing a dictionary to aggregate! i.e. functions on the row index and column names on the column index.
    Requires files:
     - token_<token_contract_address>.csv
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>.csv
    Created files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_{col}-{agg+agg2+...}_cumsum.csv
    """

    def __init__(self, session: SessionConfig, aggregations: Dict[str, Union[List[str], str]]):
        f"""
        Will be available in self
        :param session:
        :param aggregations: A dictionary of aggregations. Keys are the column names and values are the list of
                aggregation functions to be performed on the column.
        :param cumsum if true, accumulative sum up after aggregation.
        See https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.aggregate.html
        See https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.cumsum.html
        """
        super().__init__(session=session, aggregations=aggregations)

    def do_work(self):
        """
        Loads the transfers data groups by bin_duration and aggregates using count. Stores the resulting data as csv.
        transfers_<token_contract_address>_<from_block_number>_<until_block_number>_{col}-{agg1+agg2+...}_cumsum.csv
        """
        data_dir = self.session.args['data_dir']
        token_contr_addr = self.session.args['token_contract_address']
        from_blk_nr = self.session.args['from_block_number']
        until_blk_nr = self.session.args['until_block_number']
        
        df_token = load_token_data(data_dir, token_contr_addr)
        df_token_transfers = load_token_transfer_data(data_dir, token_contr_addr, from_blk_nr, until_blk_nr)

        df_token_transfers = apply_decimal_correction_to_token_transfers_value(df_token_transfers, df_token)

        df_token_transfers = df_token_transfers.loc[(from_blk_nr,):(until_blk_nr,), :]

        df_token_transfers.index = df_token_transfers.index.droplevel('block_number')
        df_token_transfers.index = pandas.to_datetime(df_token_transfers.index.to_series())

        # generate the column multi-index that will be created by this aggregate
        agg_columns = list()
        for (col, agg_s) in self.aggregations.items():
            if type(agg_s) is list:
                agg_columns.extend([(col, agg) for agg in agg_s])
            else:
                agg_columns.append((col, agg_s))

        # group by + aggregate by day and calculate the cumulative sum over the columns.
        df_token_transfers_day_agg_cumsum = df_token_transfers \
            .groupby(pandas.Grouper(level='block_timestamp', freq='D')) \
            .aggregate(self.aggregations) \
            .cumsum()

        # print(df_token_transfers_day_agg_cumsum)
        df_token_transfers_day_agg_cumsum.info()

        for col_name, col_agg_s in self.aggregations.items():
            aggs_str = '+'.join(col_agg_s) if type(col_agg_s) is list else col_agg_s
            col_index = [(c_n, col_agg) for (c_n, col_agg) in agg_columns if c_n == col_name]
            df_token_transfers_day_agg_cumsum.loc[:, col_index].to_csv(
                data_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_{col_name}-{aggs_str}_cumsum.csv"
            )
