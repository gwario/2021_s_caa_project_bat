from typing import Union, List, Dict

import pandas
from matplotlib import pyplot as plt
from matplotlib.dates import DateFormatter, YearLocator, MonthLocator

from utils.session_config import SessionConfig
from utils.task import Task


class VisualizeAggregatedCumsumTransfers(Task):
    """
    Visualizes the aggregated cumsum transfers.
    This task can be used multiple times when a unique task_id is provided.
    NOTE: the result is as described in https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.aggregate.html
    Requires files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_{col}-{agg+agg2+...}_cumsum.csv
    Created files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_{col1}-{agg1}_cumsum_lg{_task_id}.png
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_{col1}-{agg2}_cumsum_lg{_task_id}.png
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_{col2}-{agg1}_cumsum_lg{_task_id}.png
     - ...
    """

    def __init__(self, session: SessionConfig, task_id: str, aggregations: Dict[str, Union[List[str], str]],
                 logx: bool = False, logy: bool = False):
        f"""
        Will be available in self
        :param session:
        :param task_id:
        :param aggregations: A dictionary of aggregations. Keys are the column names and values are the list of
                aggregation functions to be performed on the column.
        :param logy: Logarithmic scale on the y if true
        :param logx: Logarithmic scale on the x if true
        See https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.html
        """
        super().__init__(session=session, task_id=task_id, aggregations=aggregations, logx=logx, logy=logy)

    def do_work(self):
        """
        Creates a line plot of the aggregated cumsum of transfers.
        """
        data_dir = self.session.args['data_dir']
        reports_dir = self.session.args['reports_dir']
        token_contr_addr = self.session.args['token_contract_address']
        from_blk_nr = self.session.args['from_block_number']
        until_blk_nr = self.session.args['until_block_number']

        # load files according to agg string
        df_token_transfers_day_agg_cumsum = pandas.DataFrame()
        for col_name, col_agg_s in self.aggregations.items():
            aggs_str = '+'.join(col_agg_s) if type(col_agg_s) is list else col_agg_s
            expected_headers = [0, 1] if type(col_agg_s) is list else 0
            file = data_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_{col_name}-{aggs_str}_cumsum.csv"
            fp = file.open()
            df = pandas.read_csv(fp, index_col=0, header=expected_headers)
            fp.close()
            df_token_transfers_day_agg_cumsum = pandas.concat([df_token_transfers_day_agg_cumsum, df], axis=1)

        df_token_transfers_day_agg_cumsum.info()

        # add date (without time) of block_timestamp as a top level index
        date_idx_level = 0
        idx = df_token_transfers_day_agg_cumsum.index.to_frame()
        idx.insert(date_idx_level, 'block_date', pandas.to_datetime(df_token_transfers_day_agg_cumsum.index.to_series()).dt.date)
        df_token_transfers_day_agg_cumsum.index = pandas.MultiIndex.from_frame(idx)
        # drop the date time index since it is not necessary and breaks the figure
        df_token_transfers_day_agg_cumsum = df_token_transfers_day_agg_cumsum.droplevel(1)

        px = 1 / plt.rcParams['figure.dpi']
        fig_size = (1920 * px, 1080 * px)

        for col_name, agg in df_token_transfers_day_agg_cumsum.columns.values:
            fig, ax = plt.subplots(figsize=fig_size)
            df_token_transfers_day_agg_cumsum \
                .loc[:, (col_name, agg)] \
                .plot(logx=self.logx, xlabel='Time', logy=self.logy, ylabel=f"{col_name} {agg}", ax=ax)

            ax.set_xticklabels(ax.get_xticks(), rotation=90)
            ax.xaxis.set_major_formatter(DateFormatter("%Y-%m"))
            ax.xaxis.set_major_locator(YearLocator())
            ax.xaxis.set_minor_formatter(DateFormatter("%m"))
            ax.xaxis.set_minor_locator(MonthLocator())
            plt.grid(b=True, which='both', axis='both', linestyle='-')
            fig.suptitle('Cumulative evolution of transfers over time')
            fig.tight_layout()
            fig.savefig(reports_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_{col_name}-{agg}_cumsum_lg{self.get_task_suffix()}.png")

        plt.close()
