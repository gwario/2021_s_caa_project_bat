import mplfinance as mplfinance
import pandas
from matplotlib import pyplot as plt

from utils.data import load_token_prices_data, load_token_transfer_data
from utils.session_config import SessionConfig
from utils.task import Task


class VisualizePrices(Task):
    """
    Visualizes the token prices.
    Requires files:
     - prices_<token_contract_address>_<from_block_number>_<until_block_number>.csv
    Created files:
     - prices_<token_contract_address>_<from_block_number>_<until_block_number>.png
    """

    def __init__(self, session: SessionConfig, volume: bool = False, logx: bool = False, logy: bool = False):
        f"""
        Will be available in self
        :param session:
        :param volume: show volume axis
        :param logy: Logarithmic scale on the y if true
        :param logx: Logarithmic scale on the x if true
        See https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.plot.html
        """
        super().__init__(session=session, volume=volume, logx=logx, logy=logy)

    def do_work(self):
        """
        Creates a line plot of the aggregated of transfers.
        """
        data_dir = self.session.args['data_dir']
        reports_dir = self.session.args['reports_dir']
        token_contr_addr = self.session.args['token_contract_address']
        from_blk_nr = self.session.args['from_block_number']
        until_blk_nr = self.session.args['until_block_number']

        df_token_prices = load_token_prices_data(data_dir, token_contr_addr, from_blk_nr, until_blk_nr)
        df_token_prices.index = pandas.to_datetime(df_token_prices.index.to_series())

        df_token_transfers = load_token_transfer_data(data_dir, token_contr_addr, from_blk_nr, until_blk_nr)
        df_token_transfers = df_token_transfers.loc[(from_blk_nr,):(until_blk_nr,), :]
        df_token_transfers.index = df_token_transfers.index.droplevel('block_number')
        df_token_transfers.index = pandas.to_datetime(df_token_transfers.index.to_series())
        date_index = pandas.to_datetime(df_token_transfers.index.to_series()).dt.date
        df_token_prices = df_token_prices.loc[date_index.min():date_index.max(), :]
        del df_token_transfers

        df_token_prices.info()

        px = 1 / plt.rcParams['figure.dpi']
        fig_size = (1920 * px, 1080 * px)


        # df_token_prices.plot(logx=self.logx, xlabel='Time', logy=self.logy, ylabel=f"Price", ax=ax)
        df_token_prices.rename(columns={'open': 'Open',
                                        'close': 'Close',
                                        'high': 'High',
                                        'low': 'Low',
                                        'volumeto': 'Volume'}, level=1, inplace=True)
        df_token_prices = df_token_prices.drop('volumefrom', axis=1, level=1)
        df_token_prices = df_token_prices.drop('conversionSymbol', axis=1, level=1)
        df_token_prices = df_token_prices.drop('conversionType', axis=1, level=1)

        for pair in df_token_prices.columns.get_level_values(0):
            # fig, ax = plt.subplots(figsize=fig_size)
            # print(df_token_prices.loc[:, (pair,)])
            df = df_token_prices.loc[:, (pair,)]
            mplfinance.plot(df, type='candle', volume=self.volume, figsize=fig_size, tight_layout=True,
                            axtitle=f'Evolution of the {pair} price over time',
                            savefig=reports_dir / f"prices_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_lg_{pair}.png",
                            show_nontrading=True)

        plt.close()