import time

import networkx
import pandas

from utils.session_config import SessionConfig
from utils.task import Task


class CalculateTokenNetGraphCoreDecomposition(Task):
    """
    Calculates core decomposition of the full transfer (simple undirected) graph.
    NOTE: The innermost core is the maximal subgraph of nodes with degree >= k for the largest existing k
    NOTE: The Number of cores is the largest existing k
    Metrics:
     - Number of cores
     - Number of nodes in the innermost core
     - Number of edges in the innermost core undirected single graph (without self-loops)
    Requires files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_mdg.gpickle
    Created files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_cd_full.csv
    """

    def __init__(self, session: SessionConfig):
        f"""
        Will be available in self
        :param session:
        """
        super().__init__(session=session)

    def do_work(self):
        """
        Loads the transfer graph and calculates the core decomposition. Stores the resulting data as csv.
        transfers_<token_contract_address>_<from_block_number>_<until_block_number>_cd_full.csv
        """
        data_dir = self.session.args['data_dir']
        token_contr_addr = self.session.args['token_contract_address']
        from_blk_nr = self.session.args['from_block_number']
        until_blk_nr = self.session.args['until_block_number']

        start_time = time.perf_counter()

        g_orig = networkx.read_gpickle(data_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_mdg.gpickle")
        start_time = Task.print_track("Loading the graph", start_time)

        # extract relevant time frame by block number
        # filter edges in block range
        def block_range_filter(s, t, k):
            return from_blk_nr <= g_orig.edges[s, t, k]['block_number'] <= until_blk_nr

        g_edge_restricted = networkx.subgraph_view(g_orig, filter_edge=block_range_filter)
        nodes = set()
        for u, v in g_edge_restricted.edges():
            nodes.add(u)
            nodes.add(v)
        # get view of graph with edges and nodes removed
        g_restricted = networkx.subgraph_view(g_orig, filter_node=lambda n: n in nodes, filter_edge=block_range_filter)
        start_time = Task.print_track("Filtering the target block range", start_time)

        g = networkx.Graph(g_restricted)
        del g_restricted
        start_time = Task.print_track("Creation of G", start_time)
        g.remove_edges_from(networkx.selfloop_edges(g))
        start_time = Task.print_track("Removal of self-loops in G", start_time)

        ks = networkx.core_number(g)
        start_time = Task.print_track("Calculation of core numbers in G", start_time)

        k_max_key = max(ks.keys(), key=lambda n: ks[n])
        k_max = ks[k_max_key]
        innermost_core = networkx.k_core(g, core_number=ks)
        start_time = Task.print_track("Creation of the innermost core in G", start_time)

        num_nodes_g = networkx.number_of_nodes(innermost_core)
        start_time = Task.print_track("Calculation of the number of nodes of the innermost core in G", start_time)

        num_edges_g = networkx.number_of_edges(innermost_core)
        Task.print_track("Calculation of the number of edges of the innermost core in G", start_time)

        df = pandas.DataFrame(columns=['cores (Graph)', 'innermost core nodes (Graph)', 'innermost core edges (Graph)'])
        df.loc[1, :] = (k_max, num_nodes_g, num_edges_g)
        df.to_csv(data_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_cd_full.csv")
