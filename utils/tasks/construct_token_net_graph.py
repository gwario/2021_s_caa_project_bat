import networkx
import pandas

from utils.data import load_token_transfer_data, load_token_data, apply_decimal_correction_to_token_transfers_value
from utils.session_config import SessionConfig
from utils.task import Task


class ConstructTokenNetGraph(Task):
    """
    Constructs and stores the token transfer network graph.
    MultiDiGraph
    Node id: address
    Edge key: unique numeric id
    Edge attributes: value, block_timestamp, block_number
    Requires files:
     - token_<token_contract_address>.csv
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>.csv
    Created files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_mdg.gpickle
    """

    def __init__(self, session: SessionConfig):
        f"""
        Will be available in self
        :param session:
        """
        super().__init__(session=session)

    def do_work(self):
        """
        Loads the transfers data and constructs the graph. Stores the resulting data as pickle.
        See https://networkx.org/documentation/stable/reference/generated/networkx.convert_matrix.from_pandas_edgelist.html#networkx-convert-matrix-from-pandas-edgelist
        transfers_<token_contract_address>_<from_block_number>_<until_block_number>_mdg.gpickle
        """
        data_dir = self.session.args['data_dir']
        token_contr_addr = self.session.args['token_contract_address']
        from_blk_nr = self.session.args['from_block_number']
        until_blk_nr = self.session.args['until_block_number']

        df_token = load_token_data(data_dir, token_contr_addr)
        df_token_transfers = load_token_transfer_data(data_dir, token_contr_addr, from_blk_nr, until_blk_nr)

        df_token_transfers = apply_decimal_correction_to_token_transfers_value(df_token_transfers, df_token)

        df_token_transfers = df_token_transfers.loc[(from_blk_nr,):(until_blk_nr,), :]

        df_token_transfers.index = df_token_transfers.index.set_levels(
            pandas.to_datetime(df_token_transfers.index.levels[1].to_series()),
            level=1
        )

        df_token_transfers.info()
        df_token_transfers.reset_index(inplace=True)  # move block_number and block_timestamp index to columns
        df_token_transfers.reset_index(inplace=True)  # move numeric index to column for edge key
        df_token_transfers.info()

        g = networkx.from_pandas_edgelist(df_token_transfers, source='from_address', target='to_address',
                                          create_using=networkx.MultiDiGraph, edge_key='index',
                                          edge_attr=['value', 'block_number', 'block_timestamp'])

        networkx.write_gpickle(g, data_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_mdg.gpickle")
