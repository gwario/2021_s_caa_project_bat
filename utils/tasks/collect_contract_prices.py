import itertools
import math
from typing import Dict, Union, List

import numpy
import pandas
import requests

from utils.data import load_token_transfer_data
from utils.session_config import SessionConfig
from utils.task import Task


class CollectContractPrices(Task):
    """
    Requires files:
     - secrets.json
    Created files:
     - prices_<token_contract_address>_<from_block_number>_<until_block_number>.csv
    """

    def __init__(self, session: SessionConfig, secrets: Dict,
                 fsym: Union[List[str], str] = None, tsym: Union[List[str], str] = None):
        """
        Will be available in self
        :param session:
        :param secrets:
        :param fsym: source symbol(s), default is '<token_ticker>'. you can also specify e.g. eth and btc in addition.
        :param tsym: target symbol(s), default is ['EUR', 'USD', 'ETH', 'BTC']
        """
        super().__init__(session=session, secrets=secrets, fsym=fsym, tsym=tsym)

    def do_work(self):
        """
        Iteratively queries all data for the given contract and time frame and store it as csv.
        prices_<token_contract_address>_<from_block_number>_<until_block_number>.csv
        """
        data_dir = self.session.args['data_dir']
        token_contr_addr = self.session.args['token_contract_address']
        token_ticker = self.session.args['token_ticker']
        from_blk_nr = self.session.args['from_block_number']
        until_blk_nr = self.session.args['until_block_number']

        crypto_compare_api_key = self.secrets['crypto_compare']['api_key']
        crypto_compare_historical_price_api_limit = 1800
        crypto_compare_historical_price_api_url = 'https://min-api.cryptocompare.com/data/v2/histoday'
        crypto_compare_historical_price_api_fields = ['high', 'low', 'open', 'close', 'volumefrom', 'volumeto',
                                                      'conversionSymbol', 'conversionType']

        if not self.fsym:
            self.fsym = [token_ticker]
        elif type(self.fsym) is str:
            self.fsym = [self.fsym]

        if not self.tsym:
            self.tsym = ['EUR', 'USD', 'BTC', 'ETH']
        elif type(self.tsym) is str:
            self.tsym = [self.tsym]

        trading_pairs = itertools.product(self.fsym, self.tsym)
        trading_pairs = list(filter(lambda ft: ft[0] != ft[1], trading_pairs)) # exclude where fsym equals tsym

        idx = pandas.MultiIndex.from_product([[f"{f}-{t}" for f, t in trading_pairs],
                                              crypto_compare_historical_price_api_fields])

        df_token_transfers = load_token_transfer_data(data_dir, token_contr_addr, from_blk_nr, until_blk_nr)
        df_token_transfers = df_token_transfers.loc[(from_blk_nr,):(until_blk_nr,), :]
        df_token_transfers.index = df_token_transfers.index.droplevel('block_number')
        df_token_transfers.index = pandas.to_datetime(df_token_transfers.index.to_series())

        date_index = pandas.to_datetime(df_token_transfers.index.to_series()).dt.date
        # create an artificial date index as dataframe
        df_prices = pandas.DataFrame(index=pandas.date_range(date_index.min(), date_index.max(), freq='D').to_series(),
                                     columns=idx)

        print('Fetching token prices...')
        time_frame = numpy.flip(df_prices.index.values)
        parts_cnt = math.ceil(len(time_frame) / crypto_compare_historical_price_api_limit)
        time_frame_parts = numpy.array_split(time_frame, parts_cnt)

        with requests.Session() as s:
            s.headers.update({'authorization': f'ApiKey {crypto_compare_api_key}'})

            for trading_pair in trading_pairs:
                for time_frame_part in time_frame_parts:
                    to_ts_dt = time_frame_part[0]
                    to_ts = (to_ts_dt - pandas.Timestamp("1970-01-01")) // pandas.Timedelta('1s')
                    fsym = trading_pair[0]
                    tsym = trading_pair[1]
                    resp = s.get(crypto_compare_historical_price_api_url, params={'fsym': fsym, 'tsym': tsym,
                                                                                  'toTs': to_ts,
                                                                                  # since it returns one more than limit
                                                                                  'limit': len(time_frame_part) - 1,
                                                                                  'extraParams': 'EthTokenAnalyser'})

                    ohlcvs = resp.json()['Data']['Data']
                    for ohlcv in ohlcvs:
                        for field in crypto_compare_historical_price_api_fields:
                            ts = ohlcv['time']
                            ts_dt = pandas.to_datetime(ts, unit='s', origin='unix')
                            df_prices.loc[ts_dt, (f'{fsym}-{tsym}', field)] = ohlcv[field]

        df_prices.info()
        # store data
        df_prices.to_csv(data_dir / f'prices_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}.csv')
        print('Token prices saved.')
