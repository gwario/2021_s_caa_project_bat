from typing import List, Dict, Union

import pandas

from utils.data import load_token_transfer_data, load_token_data, apply_decimal_correction_to_token_transfers_value
from utils.session_config import SessionConfig
from utils.task import Task


class CalculateAggregatedTransfers(Task):
    """
    Aggregates transfers over the whole data.
    This task can be used multiple times when a unique task_id is provided.
    NOTE: the result is as described in https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.aggregate.html
    when passing a dictionary to aggregate! i.e. functions on the row index and column names on the column index.
    Requires files:
     - token_<token_contract_address>.csv
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>.csv
    Created files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_{col}-{agg+agg2+...}_full{_task_id}.csv
    """

    def __init__(self, session: SessionConfig, task_id: str, aggregations: Dict[str, Union[List[str], str]]):
        f"""
        Will be available in self
        :param session:
        :param task_id:
        :param aggregations: A dictionary of aggregations. Keys are the column names and values are the list of
        aggregation functions to be performed on the column.
        See https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.aggregate.html
        """
        super().__init__(session=session, task_id=task_id, aggregations=aggregations)

    def do_work(self):
        """
        Loads the transfers data groups by bin_duration and aggregates using count. Stores the resulting data as csv.
        transfers_<token_contract_address>_<from_block_number>_<until_block_number>_{col}-{agg1+agg2+...}_full{_task_id}.csv
        """
        data_dir = self.session.args['data_dir']
        token_contr_addr = self.session.args['token_contract_address']
        from_blk_nr = self.session.args['from_block_number']
        until_blk_nr = self.session.args['until_block_number']
        
        df_token = load_token_data(data_dir, token_contr_addr)
        df_token_transfers = load_token_transfer_data(data_dir, token_contr_addr, from_blk_nr, until_blk_nr)

        df_token_transfers = apply_decimal_correction_to_token_transfers_value(df_token_transfers, df_token)

        df_token_transfers = df_token_transfers.loc[(from_blk_nr,):(until_blk_nr,), :]

        df_token_transfers.index = df_token_transfers.index.droplevel('block_number')
        df_token_transfers.index = pandas.to_datetime(df_token_transfers.index.to_series())

        df_token_transfers_agg_full = df_token_transfers.aggregate(self.aggregations)

        df_token_transfers_agg_full.info()

        for col_name, col_agg_s in self.aggregations.items():
            aggs_str = '+'.join(col_agg_s) if type(col_agg_s) is list else col_agg_s
            aggs_index = col_agg_s if type(col_agg_s) is list else [col_agg_s]
            df_token_transfers_agg_full.loc[aggs_index, [col_name]].to_csv(
                data_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_{col_name}-{aggs_str}_full{self.get_task_suffix()}.csv"
            )
