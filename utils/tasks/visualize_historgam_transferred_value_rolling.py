from typing import Tuple, Any, Optional, Union

import numpy
import pandas
from matplotlib import pyplot as plt, cm

from utils.data import load_token_transfer_data, load_token_data, apply_decimal_correction_to_token_transfers_value
from utils.session_config import SessionConfig
from utils.task import Task


class VisualizeHistogramTransferredValueRolling(Task):
    """
    Visualized the transfers value over the whole data with a rolling window.
    This task can be used multiple times when a unique task_id is provided.
    NOTE: the result is as described in https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.aggregate.html
    Requires files:
     - token_<token_contract_address>.csv
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>.csv
    Created files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_<window>-<step>_hg-value[_<task_id>].png
    """

    def __init__(self, session: SessionConfig, window_size_step_width_days: Tuple[int, Optional[int]],
                 task_id: str = None, logx: bool = False, logy: bool = False, rwidth: float = None,
                 bins: Any = None, boundaries: Tuple[Union[float, None], Union[float, None]] = None):
        f"""
        Will be available in self
        :param task_id: per task unique id
        :param session:
        :param logy: Logarithmic scale on the y if true
        :param logx: Logarithmic scale on the x if true
        :param rwidth: 
        :param bins:  
        :param boundaries: range argument of .hist()
        :param window_size_step_width_days: tuple of window size and step width in days; if step width is none, non overlapping windows;
         if None for whole time span
        See https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.hist.html#matplotlib.axes.Axes.hist
        """
        super().__init__(task_id=task_id, session=session, logx=logx, logy=logy, rwidth=rwidth, bins=bins, boundaries=boundaries,
                         window_size_step_width_days=window_size_step_width_days)

    def do_work(self):
        """
        Creates a histogram of the transferred values.
        """
        data_dir = self.session.args['data_dir']
        reports_dir = self.session.args['reports_dir']
        token_contr_addr = self.session.args['token_contract_address']
        from_blk_nr = self.session.args['from_block_number']
        until_blk_nr = self.session.args['until_block_number']
        step_width_days, window_size_days = self.get_window_step_params(self.window_size_step_width_days)

        df_token = load_token_data(data_dir, token_contr_addr)
        df_token_transfers = load_token_transfer_data(data_dir, token_contr_addr, from_blk_nr, until_blk_nr)

        df_token_transfers = apply_decimal_correction_to_token_transfers_value(df_token_transfers, df_token)

        df_token_transfers = df_token_transfers.loc[(from_blk_nr,):(until_blk_nr,), :]

        df_token_transfers.index = df_token_transfers.index.droplevel('block_number')
        df_token_transfers.index = pandas.to_datetime(df_token_transfers.index.to_series())

        # add date (without time) of block_timestamp as a top level index
        date_idx_level = 0
        idx = df_token_transfers.index.to_frame()
        idx.insert(date_idx_level, 'block_date', pandas.to_datetime(df_token_transfers.index.to_series()).dt.date)
        df_token_transfers.index = pandas.MultiIndex.from_frame(idx)

        # does not work / not yet supported - https://github.com/pandas-dev/pandas/issues/34642
        # for df_window in df_token_transfers \
        #         .groupby(level=0) \
        #         .rolling(30, on=df_token_transfers.index.levels[0]):

        # create an artificial date index as dataframe, then roll over it then
        # take the specific windows (idx % step ==0) that may generate an overlap
        virtual_date_index = pandas.date_range(
            df_token_transfers.index.levels[date_idx_level].min(),
            df_token_transfers.index.levels[date_idx_level].max(),
            freq='D'
        )
        virtual_date_index_series = virtual_date_index.to_series()
        virtual_date_index_series_rolling = virtual_date_index_series.rolling(window=window_size_days)

        # when bins is a number, the bin sizes are created with equal width for every individual hist call
        # in order for the bins to stay constant, we must convert a number here to a sequence
        actual_range = VisualizeHistogramTransferredValueRolling.get_actual_boundaries(self.boundaries,
                                                                                       df_token_transfers)

        if type(self.bins) == int:
            r = actual_range if actual_range else (df_token_transfers.value.min(), df_token_transfers.value.max())
            self.bins = numpy.linspace(numpy.float64(r[0]), numpy.float64(r[1]), num=self.bins + 1)

        plt.style.use('bmh')
        # color_schema = 'Blues'
        color_schema = 'viridis'
        alpha = 0.3
        px = 1 / plt.rcParams['figure.dpi']
        fig_size = (1920 * px, 1080 * px)
        fig, ax = plt.subplots(figsize=fig_size)

        cmap = cm.get_cmap(color_schema, len(virtual_date_index_series))
        colors = cmap.colors
        target_tick_count = 10
        tick_label_format = '%b %d,%Y'
        color_bar_ticks = pandas.date_range(
            df_token_transfers.index.levels[date_idx_level].min(),
            df_token_transfers.index.levels[date_idx_level].max(),
            periods=target_tick_count
        )
        cbar = plt.colorbar(plt.cm.ScalarMappable(cmap=cmap,
                                                  norm=plt.Normalize(
                                                      vmin=virtual_date_index_series.index.min().value,
                                                      vmax=virtual_date_index_series.index.max().value)),
                            ticks=color_bar_ticks.astype(int).values)
        cbar.ax.set_yticklabels(pandas.to_datetime(cbar.get_ticks()).strftime(date_format=tick_label_format))
        window_indices_index = 0
        first_window_indices_index = window_size_days - 1
        for window_indices in virtual_date_index_series_rolling:
            if (
                    # first window with full coverage
                    window_indices_index == first_window_indices_index
            ) or (
                    # take every ith the window only
                    window_indices_index > first_window_indices_index
                    and (window_indices_index - first_window_indices_index) % step_width_days == 0
            ):
                # find the indices for which data exists
                existing_window_indices = df_token_transfers.index.levels[0].intersection(window_indices)
                # select all dates from the window for which data exists
                df_window = df_token_transfers.loc[
                    pandas.IndexSlice[pandas.to_datetime(existing_window_indices.to_series()).dt.date, :]
                ]

                ax.hist(df_window.loc[:, 'value'].values, bins=self.bins, rwidth=self.rwidth,
                        range=actual_range, histtype="stepfilled", alpha=alpha, color=colors[window_indices_index])

            window_indices_index += 1

        if self.logx:
            ax.set_xscale('log')
        if self.logy:
            ax.set_yscale('log')

        ax.set_title('Histogram of transferred values')
        ax.set_xlabel("Value bins")
        ax.set_ylabel("Value count")
        fig.tight_layout()
        fig.savefig(
            reports_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_{window_size_days}-{step_width_days}_hg-value{self.get_task_suffix()}.png")
        plt.close()

    @staticmethod
    def get_window_step_params(window_size_days_step_width_days):
        if window_size_days_step_width_days:
            window_size_days, step_width_days = window_size_days_step_width_days
            if not step_width_days:
                step_width_days = window_size_days
        else:
            window_size_days = None
            step_width_days = None
        return step_width_days, window_size_days

    @staticmethod
    def get_actual_boundaries(boundaries, df_window):
        if boundaries:
            actual_range = (boundaries[0] if boundaries[0] else df_window.value.min(),
                            boundaries[1] if boundaries[1] else df_window.value.max())
        else:
            actual_range = None
        return actual_range
