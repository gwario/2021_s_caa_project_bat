import time
from typing import Optional, Tuple

import networkx
import pandas

from utils.data import load_token_data, load_token_transfer_data, apply_decimal_correction_to_token_transfers_value
from utils.session_config import SessionConfig
from utils.task import Task


class CalculateTokenNetGraphCoreDecompositionRolling(Task):
    """
    Calculates core decomposition of the transfer (simple undirected) graph within a rolling window.
    NOTE: The innermost core is the maximal subgraph of nodes with degree >= k for the largest existing k
    NOTE: The Number of cores is the largest existing k
    Metrics:
     - Number of cores
     - Number of nodes in the innermost core
     - Number of edges in the innermost core undirected single graph (without self-loops)
    Requires files:
     - token_<token_contract_address>.csv
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>.csv
    Created files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_cd_roll-<window_size_days>d-<shift_size_days>d.csv
    """

    def __init__(self, session: SessionConfig, window_size_step_width_days: Tuple[int, Optional[int]]):
        f"""
        Will be available in self
        :param session:
        :param window_size_step_width_days: tuple of window size and step width in days; if step width is none, non
         overlapping windows; if None for whole time span
        """
        super().__init__(session=session, window_size_step_width_days=window_size_step_width_days)

    def do_work(self):
        """
        Loads the transfer graph and calculates the core decomposition. Stores the resulting data as csv.
        transfers_<token_contract_address>_<from_block_number>_<until_block_number>_cd_roll-<window_size_days>d-<shift_size_days>d.csv
        """
        data_dir = self.session.args['data_dir']
        token_contr_addr = self.session.args['token_contract_address']
        from_blk_nr = self.session.args['from_block_number']
        until_blk_nr = self.session.args['until_block_number']
        step_width_days, window_size_days = CalculateTokenNetGraphCoreDecompositionRolling.get_window_step_params(
            self.window_size_step_width_days)

        start_time = time.perf_counter()

        df_token = load_token_data(data_dir, token_contr_addr)
        start_time = Task.print_track("Loading token info", start_time)
        df_token_transfers = load_token_transfer_data(data_dir, token_contr_addr, from_blk_nr, until_blk_nr)
        start_time = Task.print_track("Loading token transfers", start_time)
        df_token_transfers = apply_decimal_correction_to_token_transfers_value(df_token_transfers, df_token)
        start_time = Task.print_track("Correction of value decimals", start_time)

        df_token_transfers = df_token_transfers.loc[(from_blk_nr,):(until_blk_nr,), :]

        df_token_transfers.index = df_token_transfers.index.droplevel('block_number')
        df_token_transfers.index = pandas.to_datetime(df_token_transfers.index.to_series())

        # add date (without time) of block_timestamp as a top level index
        date_idx_level = 0
        idx = df_token_transfers.index.to_frame()
        idx.insert(date_idx_level, 'block_date', pandas.to_datetime(df_token_transfers.index.to_series()).dt.date)
        df_token_transfers.index = pandas.MultiIndex.from_frame(idx)

        # create an artificial date index as dataframe, then roll over it then
        # take the specific windows (idx % step ==0) that may generate an overlap
        virtual_date_index = pandas.date_range(
            df_token_transfers.index.levels[date_idx_level].min(),
            df_token_transfers.index.levels[date_idx_level].max(),
            freq='D'
        )
        virtual_date_index_series = virtual_date_index.to_series()
        virtual_date_index_series_rolling = virtual_date_index_series.rolling(window=window_size_days)
        start_time = Task.print_track("Creation of date index", start_time)

        df = pandas.DataFrame(
            columns=['# Cores (Graph)', '# Nodes in Innermost Core (Graph)', '# Edges in Innermost Core (Graph)'])

        window_indices_index = 0
        first_window_indices_index = window_size_days - 1
        for window_indices in virtual_date_index_series_rolling:
            if (
                    # first window with full coverage
                    window_indices_index == first_window_indices_index
            ) or (
                    # take every ith the window only
                    window_indices_index > first_window_indices_index
                    and (window_indices_index - first_window_indices_index) % step_width_days == 0
            ):
                last_date_window = window_indices[-1]
                # find the indices for which data exists
                existing_window_indices = df_token_transfers.index.levels[0].intersection(window_indices)
                # select all dates from the window for which data exists
                df_window = df_token_transfers.loc[pandas.IndexSlice[
                                                   pandas.to_datetime(existing_window_indices.to_series()).dt.date, :]]
                df_window.reset_index(inplace=True)  # move block_number and block_timestamp index to columns
                df_window.reset_index(inplace=True)  # move numeric index to column for edge key
                g = networkx.from_pandas_edgelist(df_window, source='from_address', target='to_address',
                                                  create_using=networkx.Graph, edge_key='index',
                                                  edge_attr=['value', 'block_date', 'block_timestamp'])
                g.remove_edges_from(networkx.selfloop_edges(g))

                ks = networkx.core_number(g)

                k_max_key = max(ks.keys(), key=lambda n: ks[n])
                k_max = ks[k_max_key]
                innermost_core = networkx.k_core(g, core_number=ks)
                num_nodes_g = networkx.number_of_nodes(innermost_core)
                num_edges_g = networkx.number_of_edges(innermost_core)

                df.loc[last_date_window, :] = (k_max, num_nodes_g, num_edges_g)

            window_indices_index += 1

        Task.print_track(f"Core decomposition of {len(df)} graphs", start_time)
        df.to_csv(data_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_cd_roll-{window_size_days}d-{step_width_days}d.csv")

    @staticmethod
    def get_window_step_params(window_size_days_step_width_days):
        if window_size_days_step_width_days:
            window_size_days, step_width_days = window_size_days_step_width_days
            if not step_width_days:
                step_width_days = window_size_days
        else:
            window_size_days = None
            step_width_days = None
        return step_width_days, window_size_days
