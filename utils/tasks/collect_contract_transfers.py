from typing import Dict

from google.cloud import bigquery
from google.oauth2 import service_account

from utils.session_config import SessionConfig
from utils.task import Task


class CollectContractTransfers(Task):
    """
    Requires files:
     - secrets.json
    Created files:
     - token_<token_contract_address>.csv
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>.csv
    """

    def __init__(self, session: SessionConfig, secrets: Dict):
        """
        Will be available in self
        :param session:
        :param secrets:
        """
        super().__init__(session=session, secrets=secrets)

    def do_work(self):
        """
        Sets up the bigquery client, fetch the data and store it as csv.
        token_<token_contract_address>.csv
        transfers_<token_contract_address>_<from_block_number>_<until_block_number>.csv
        """
        data_dir = self.session.args['data_dir']
        token_contr_addr = self.session.args['token_contract_address']
        from_blk_nr = self.session.args['from_block_number']
        until_blk_nr = self.session.args['until_block_number']

        google_service_account_key = self.secrets['google']['service_account_key']
        # See https://ethereum-etl.readthedocs.io/en/latest/limitations/
        token_query = f"""
            SELECT  t.address, t.name, t.symbol, cast(t.decimals as FLOAT64) AS decimals, t.total_supply, t.block_number,
                t.block_timestamp
            FROM `bigquery-public-data.crypto_ethereum.tokens` AS t
            WHERE t.address = '{token_contr_addr}'
            """

        # use safe_cast(value as NUMERIC) to avoid loss of precision but take the risk of overflow
        token_transfers_query = f"""
            SELECT tfs.from_address, tfs.to_address, cast(tfs.value as FLOAT64) AS value, tfs.block_number,
                tfs.block_timestamp
            FROM `bigquery-public-data.crypto_ethereum.token_transfers` AS tfs 
            WHERE tfs.block_number >= {from_blk_nr} AND 
                  tfs.block_number <= {until_blk_nr} AND 
                  tfs.token_address = '{token_contr_addr}' 
            ORDER BY tfs.block_number
            """

        # prepare google bigquery client
        credentials = service_account.Credentials.from_service_account_info(
            google_service_account_key,
            scopes=['https://www.googleapis.com/auth/bigquery']
        )
        client = bigquery.Client(credentials=credentials)
        # client = bigquery.Client()
        # client.dataset('crypto_ethereum', 'bigquery-public-data')

        # fetch data
        print('Running token query...')
        df_token = client.query(token_query).to_dataframe()
        df_token.describe()
        # store data
        df_token.to_csv(data_dir / f'tokens_{token_contr_addr}.csv')
        print('Token date saved.')

        print('Running token transfers query...')
        df_token_transfers = client.query(token_transfers_query).to_dataframe()
        df_token_transfers.describe()
        # store data
        df_token_transfers.to_csv(data_dir / f'transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}.csv')
        print('Token transfers date saved.')
