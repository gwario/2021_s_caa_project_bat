import pandas

from utils.data import load_token_transfer_data, load_token_data, apply_decimal_correction_to_token_transfers_value
from utils.session_config import SessionConfig
from utils.task import Task


class CalculateSortHeadTransfers(Task):
    """
    Sorts and returns the top x rows.
    Requires files:
     - token_<token_contract_address>.csv
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>.csv
    Created files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_{col}-{sort}-{head}_full[_task_id].csv
    """

    def __init__(self, session: SessionConfig, column: str, ascending: bool = True, head: int = 10, task_id: str = None):
        f"""
        Will be available in self
        :param session:
        :param column:
        :param ascending:
        :param head:
        :param task_id:
        See https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.sort_values.html
        """
        super().__init__(session=session, column=column, ascending=ascending, head=head, task_id=task_id)

    def do_work(self):
        """
        Loads the transfers data sorts and returns the top head rows. Stores the resulting data as csv.
        transfers_<token_contract_address>_<from_block_number>_<until_block_number>_{col}-{sort}-{head}_full[_task_id].csv
        """
        data_dir = self.session.args['data_dir']
        token_contr_addr = self.session.args['token_contract_address']
        from_blk_nr = self.session.args['from_block_number']
        until_blk_nr = self.session.args['until_block_number']
        
        df_token = load_token_data(data_dir, token_contr_addr)
        df_token_transfers = load_token_transfer_data(data_dir, token_contr_addr, from_blk_nr, until_blk_nr)

        df_token_transfers = apply_decimal_correction_to_token_transfers_value(df_token_transfers, df_token)

        df_token_transfers = df_token_transfers.loc[(from_blk_nr,):(until_blk_nr,), :]

        df_token_transfers.index = df_token_transfers.index.droplevel('block_number')
        df_token_transfers.index = pandas.to_datetime(df_token_transfers.index.to_series())

        df_token_transfers_sort_full = df_token_transfers.sort_values(by=self.column, ascending=self.ascending)
        df_token_transfers_sort_head_full = df_token_transfers_sort_full.head(self.head)

        df_token_transfers_sort_head_full.info()
        sort = 'asc' if self.ascending else 'desc'
        df_token_transfers_sort_head_full.to_csv(
            data_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_{self.column}-{sort}-{self.head}_full{self.get_task_suffix()}.csv"
        )

