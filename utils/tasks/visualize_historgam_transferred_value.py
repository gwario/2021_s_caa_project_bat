from typing import Tuple, Any, Union

import pandas
from matplotlib import pyplot as plt, cm

from utils.data import load_token_transfer_data, load_token_data, apply_decimal_correction_to_token_transfers_value
from utils.session_config import SessionConfig
from utils.task import Task


class VisualizeHistogramTransferredValue(Task):
    """
    Visualized the transfers value over the whole data.
    This task can be used multiple times when a unique task_id is provided.
    NOTE: the result is as described in https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.aggregate.html
    Requires files:
     - token_<token_contract_address>.csv
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>.csv
    Created files:
     - transfers_<token_contract_address>_<from_block_number>_<until_block_number>_full_hg-value[_<task_id>].png
    """

    def __init__(self, session: SessionConfig, task_id: str = None, logx: bool = False, logy: bool = False,
                 rwidth: float = None, bins: Any = None, boundaries: Tuple[Union[float, None],
                                                                           Union[float, None]] = None):
        f"""
        Will be available in self
        :param task_id: per task unique id
        :param session:
        :param logy: Logarithmic scale on the y if true
        :param logx: Logarithmic scale on the x if true
        :param rwidth: 
        :param bins:  
        :param boundaries: range argument of .hist()
        See https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.hist.html#matplotlib.axes.Axes.hist
        """
        super().__init__(task_id=task_id, session=session, logx=logx, logy=logy, rwidth=rwidth, bins=bins,
                         boundaries=boundaries)

    def do_work(self):
        """
        Creates a histogram of the transferred values.
        """
        data_dir = self.session.args['data_dir']
        reports_dir = self.session.args['reports_dir']
        token_contr_addr = self.session.args['token_contract_address']
        from_blk_nr = self.session.args['from_block_number']
        until_blk_nr = self.session.args['until_block_number']

        df_token = load_token_data(data_dir, token_contr_addr)
        df_token_transfers = load_token_transfer_data(data_dir, token_contr_addr, from_blk_nr, until_blk_nr)

        df_token_transfers = apply_decimal_correction_to_token_transfers_value(df_token_transfers, df_token)

        df_token_transfers = df_token_transfers.loc[(from_blk_nr,):(until_blk_nr,), :]

        df_token_transfers.index = df_token_transfers.index.droplevel('block_number')
        df_token_transfers.index = pandas.to_datetime(df_token_transfers.index.to_series())

        # add date (without time) of block_timestamp as a top level index
        date_idx_level = 0
        idx = df_token_transfers.index.to_frame()
        idx.insert(date_idx_level, 'block_date', pandas.to_datetime(df_token_transfers.index.to_series()).dt.date)
        df_token_transfers.index = pandas.MultiIndex.from_frame(idx)

        plt.style.use('bmh')
        # color_schema = 'Blues'
        color_schema = 'viridis'
        alpha = 0.3
        px = 1 / plt.rcParams['figure.dpi']
        fig_size = (1920 * px, 1080 * px)
        fig, ax = plt.subplots(figsize=fig_size)

        cmap = cm.get_cmap(color_schema, len(df_token_transfers))
        colors = cmap.colors

        actual_range = self.get_actual_boundaries(self.boundaries, df_token_transfers)

        ax.hist(df_token_transfers.loc[:, 'value'].values, bins=self.bins, rwidth=self.rwidth,
                range=actual_range, histtype="stepfilled", alpha=alpha, color=colors[int(len(colors) / 2)])

        if self.logx:
            ax.set_xscale('log')
        if self.logy:
            ax.set_yscale('log')

        ax.set_title('Histogram of transferred values')
        ax.set_xlabel("Value bins")
        ax.set_ylabel("Value count")

        fig.tight_layout()
        fig.savefig(reports_dir / f"transfers_{token_contr_addr}_{from_blk_nr}_{until_blk_nr}_full_hg-value{self.get_task_suffix()}.png")
        plt.close()

    @staticmethod
    def get_actual_boundaries(boundaries, df_window):
        if boundaries:
            actual_range = (boundaries[0] if boundaries[0] else df_window.value.min(),
                            boundaries[1] if boundaries[1] else df_window.value.max())
        else:
            actual_range = None
        return actual_range
