import time
from abc import ABC, abstractmethod
from multiprocessing import Manager
from typing import Any, Union

from utils.session_config import SessionConfig


class Task(ABC):

    def __init__(self, **params):
        """
        :param params: parameters of this task. e.g. window width (a month, a week, ...)
        NOTE: A SessionConfig has to be provided in 'session'!
        """
        self.__dict__.update(params)

    def using(self, manager: Manager) -> Any:
        """
        Initializes the task using the manager.
        :param manager:
        :return: self
        """
        if not self.session.progress.get(self.get_task_key()):
            self.session.init_task(self.get_task_key(), manager)
        return self

    @abstractmethod
    def do_work(self):
        """
        Loads, processes and stores data or visualizes data.
        """
        pass

    @staticmethod
    def print_track(message, start_time):
        """
        Prints f"{message} took {time.perf_counter() - start_time:.2f} seconds" and returns time.perf_counter()
        :param start_time: start time of the task to track as returned by time.perf_counter()
        :param message: the message
        :return: time.perf_counter()
        """
        print(f"{message} took {time.perf_counter() - start_time:.2f} seconds")
        return time.perf_counter()

    def get_task_id(self) -> Union[str, None]:
        return self.__dict__.get('task_id')

    def get_task_key(self) -> str:
        """
        :return: the task key, consisting of the task's class name and optionally a instance specific task id.
        """
        class_name = type(self).__name__
        return f'{class_name}-{self.get_task_id()}' if self.get_task_id() else f'{class_name}'

    def get_task_suffix(self) -> str:
        """
        For tasks that can occur multople times, provides a suffix for filenames.
        :return: the task_id or empty string
        """
        return f'_{self.get_task_id()}' if self.get_task_id() else ''

    def execute(self):
        if not self.__dict__.get('session') or not isinstance(self.__dict__.get('session'), SessionConfig):
            raise AttributeError("SessionConfig in parameter 'session' is missing!")

        if self.session.has_task_been_completed(self.get_task_key()):
            print(f'[{self.get_task_key()}] Already completed.')
        else:
            print(f'[{self.get_task_key()}] Executing task...')
            exec_start_time = time.perf_counter()
            try:
                self.do_work()
                exec_end_time = time.perf_counter()
                print(f'[{self.get_task_key()}] Finished after {(exec_end_time - exec_start_time):.2f} seconds!')
                self.session.update_progress_task_complete(self.get_task_key())
            except Exception as ex:
                exec_end_time = time.perf_counter()
                print(f'[{self.get_task_key()}] Execute failed after {(exec_end_time - exec_start_time):.2f}!', ex)
                raise ex
